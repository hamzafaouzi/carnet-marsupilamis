

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


//require('./bootstrap');

window.Vue = require('vue');


Vue.component('liste-des-amis-component', require('./components/ListeAmisComponent.vue'));
Vue.component('marsupilami-component', require('./components/MarsupilamiComponent.vue'));
Vue.component('listes-des-marsupilamis-component', require('./components/ListeMarsupilamisComponent'));

const app = new Vue({
    el: '#app'
});
