@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 bg-white">

                <nav class="nav nav-pills nav-justified my-3">
                    <a class="nav-item nav-link" href="/home">Liste des amis</a>
                    <a class="nav-item nav-link" href="/liste_des_marsupilamis">Toutes les Marsupilami</a>
                    <a class="nav-item nav-link" href="/informations">Informations</a>
                </nav>

                <listes-des-marsupilamis-component></listes-des-marsupilamis-component>

            </div>
        </div>
    </div>
    </div>
@endsection
