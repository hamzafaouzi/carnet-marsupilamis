<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/mes_amis', 'HomeController@getMesAmis');
Route::get('/marsupilamis', 'HomeController@getmarsupilamis');
Route::get('/liste_des_marsupilamis' , 'HomeController@listesDesMarsupilamis' );
Route::post('/ami/ajouter/{id}' , 'HomeController@ajouterAmi' )->where([ 'id' => '[0-9]*' ]);
Route::delete('/ami/supprimer/{id}' , 'HomeController@supprimerAmi' )->where([ 'id' => '[0-9]*' ]);
