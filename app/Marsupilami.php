<?php

namespace App;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

class Marsupilami extends Authenticatable
{
    use Notifiable;

    protected $table = 'marsupilamis';

    protected $fillable = [
        'nom', 'password', 'age' , 'race' , 'nourriture', 'famille'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function amis()
    {
        return $this->belongsToMany(Marsupilami::class, 'marsupilami_ami', 'marsupilami_id', 'ami_id');
    }
}
