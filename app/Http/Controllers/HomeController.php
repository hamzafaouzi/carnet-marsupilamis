<?php

namespace App\Http\Controllers;

use App\Marsupilami;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    public function getMesAmis()
    {
        return auth()->user()->amis()->get()->toArray();
    }

    public function getmarsupilamis()
    {
        return Marsupilami::orderBy('created_at')->get()->toArray();
    }

    public function listesDesMarsupilamis()
    {
        return view('liste_des_marsupilamis');
    }

    public function ajouterAmi($amiId)
    {
        $nouveauAmi = Marsupilami::findOrFail($amiId);

        $relationExist = DB::table('marsupilami_ami')
            ->where('marsupilami_id' , auth()->id() )
            ->where('ami_id' , $amiId)
            ->exists();

        if (!$relationExist)
            auth()->user()->amis()->save($nouveauAmi);


        return response(null , Response::HTTP_OK);

    }

    public function supprimerAmi($amiId)
    {

        $relationExist = DB::table('marsupilami_ami')
            ->where('marsupilami_id' , auth()->id() )
            ->where('ami_id' , $amiId)
            ->exists();

        if ($relationExist)
            DB::table('marsupilami_ami')
                ->where('Marsupilami_id' , auth()->id() )
                ->where('ami_id' , $amiId)
                ->delete();

        return response(null , Response::HTTP_OK);
    }

}

