# Carnet d'addresse Marsupilami README
 
### Tech
- Laravel 5.7
- Vue.js
- Bootstrap 4 

### Laravel Requirement
- PHP \>= 7.0.0
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
for more info please visit [ Laravel Documentation website ](https://laravel.com/)
## Installation
-  Navigate To Project Folder :

-  Set the basic configurations :

	cp .env.example .env

- Install the Laravel extended repositories:

	composer install

-  Generate Web App Key
	php artisan key:generate 

-  Clear Cache
	php artisan cache:clear



