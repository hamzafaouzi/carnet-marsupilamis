<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarsupilamiAmiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marsupilami_ami', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marsupilami_id')->unsigned();;
            $table->integer('ami_id')->unsigned();;

            $table->foreign('marsupilami_id')->references('id')->on('marsupilamis');
            $table->foreign('ami_id')->references('id')->on('marsupilamis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marsupilami_ami');
    }
}
