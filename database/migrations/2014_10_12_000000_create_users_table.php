<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marsupilamis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom')->unique();
            $table->string('famille')->nullable(true);
            $table->smallInteger('age')->nullable(true);
            $table->string('nourriture')->nullable(true);
            $table->string('race')->nullable(true);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marsupilamis');
    }
}
